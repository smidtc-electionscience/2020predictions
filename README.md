A write up of the 2020 Michigan voter file predictions and what I have available to download are provided [here](https://smidtc.gitlab.io/blog/). 

A record of the predicted probability for all 7.8 million voters is provided [here](https://gitlab.com/smidtc-electionscience/2020predictions/-/blob/master/individualpredictions.csv). Note that the data has no personal information except the State of Michigan's Voter ID number. 

A **summary assessment** of the 2018 predictions using this method are provided in this [image of a poster](https://gitlab.com/smidtc-electionscience/2018predictions/-/blob/master/finalposter.png) I presented at the 2019 Annual Meeting of the American Associate of Public Opinion Researchers. Files from 2018 are also available [here](https://gitlab.com/smidtc-electionscience/2018predictions/-/tree/master).
